module.exports = class {
  
  inEnglish(number) {
    if (isNaN(number)) {
      throw new Error('Argument not a number.')
    }

    if (number < 0 || number > 999999999999) {
      throw new Error('Number must be between 0 and 999,999,999,999.')
    }

    var sayNumbersMap = new Object()
    sayNumbersMap.singleDigits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    sayNumbersMap.doubleDigits = ['twenty-', 'thirty-', 'forty-', 'fifty-', 'sixty-', 'seventy-', 'eighty-', 'ninety-']
    sayNumbersMap.doubleDigitsExceptions = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    sayNumbersMap.scales = ['hundred', 'thousand', 'million', 'billion']

    if (number < 1) {
      return sayNumbersMap.singleDigits[0]
    }
    
    var say = []
    var numbers = ("000000000000" + number).slice(-12).match(/.{1,3}/g)

    while (numbers.length) {
      if (numbers[0] == 0) {
        numbers.shift()
        continue
      }
      
      // add hundreds
      if (numbers[0].charAt(0) != 0) {
        say.push(sayNumbersMap.singleDigits[parseInt(numbers[0].charAt(0))])
        say.push(sayNumbersMap.scales[0])
      }

      // Use and (correctly) when spelling out the number in English
      if (number.length < 6 && numbers.length < 2) {
        say.push('and')
      }

      // add tens
      if (numbers[0].slice(-2) > 9 && numbers[0].slice(-2) < 20) {
        say.push(sayNumbersMap.doubleDigitsExceptions[numbers[0].charAt(2)])
      } else {
        if (numbers[0].charAt(1) != 0) {
          say.push(sayNumbersMap.doubleDigits[numbers[0].charAt(1) - 2])
        }
  
        if (numbers[0].charAt(2) != 0) {
          say.push(sayNumbersMap.singleDigits[parseInt(numbers[0].charAt(2))])
        }
      }
      
      // add number scale
      if (numbers.length > 1) {
        say.push(sayNumbersMap.scales[numbers.length - 1])
      }

      numbers.shift()
    }

    return say.join(' ').trim().replace(/(-| and)$/g, '').replace(/- /g, '-')
  }

}