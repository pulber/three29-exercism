<?php

function calculate($string) {
  $string = str_replace(['plus', 'minus', 'multiplied', 'divided'], ['+', '-', '*', '/'], $string);
  $string = preg_replace('/[^-?0-9\+\-\*\/]/', '', $string);
  $pattern = '/(-?[0-9]{1,})([\+\-\*\/])(-?[0-9]{1,})/';
  
  if (!preg_match($pattern, $string)) {
    throw new InvalidArgumentException('Invalid operator.');
  }
  
  while(preg_match($pattern, $string, $match)) {
    list($current_match, $num_1, $operator, $num_2) = $match;

    $result = 0;
    
    switch($operator) {
      case '+':
        $result += $num_1 + $num_2;
        break;
      case '-':
        $result += $num_1 - $num_2;
        break;
      case '*':
        $result += $num_1 * $num_2;
        break;
      case '/':
        $result += $num_1 / $num_2;
        break;
    }

    $string = str_replace($current_match, $result, $string);
  }
  
  return $result;
}
