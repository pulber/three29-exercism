<?php

class Series {

  public $digits;

  function __construct($digits) {
    $this->digits = (string) $digits;
  }

  public function largestProduct($n) {
    if (!is_numeric($n) || $n <= 0) {
      throw new InvalidArgumentException('Invalid argument.');
    }
    
    $number = $this->toDecimal($this->digits);
    $number_array = str_split($number);
    $max_sum = 0;
    
    while(count($number_array) >= $n) {
      $sum = array_product(array_slice($number_array, 0, $n));
      
      if ($sum > $max_sum) {
        $max_sum = $sum;
      }

      array_shift($number_array);
    }

    return $max_sum;
  }

  private function toDecimal($string) {
    // check if octal
    if (preg_match('/(^0[0-7]{2,})([0-9]{0,})/', $string, $match)) {
      return (string) octdec($match[1]) . $match[2];
    }

    return $string;
  }

}
